Nazi Zombies Portable: Unofficial Patch 1.0.3
====

This is the official changelog provided from the Unofficial Patch Repositories.

What's New?
----

###Implementations

- Implemented a brand new ingame changelog menu.

###Reimplementations

- Reimplemented the Gewehr.

- Reimplemented the FG42.

- Reimplemented the Raygun.

###Tweaks

- Fixed player speed when the player is downed.

- Fixed persistent "Press to turn on power" message while the power being on.

- Fixed a typo at the Controls Settings menu. (A-NUB Strafing)

- Fixed Perk buying bugs. (Allowed player to buy another perk while drinking a perk, or purchase a weapon)

- Fixed graphical issues in the controls menu.

- Fixed splash image, it had Press Start drawn.

- Fixed the Button icons as they were full transparent.

- Fixed minor pixel alteration in charset.

###Removals

- Removed A-NUB Movement, as it was an unfair advantage for the player gameplay.

- Removed "use" icon from appearing when touching a perk machine while the power is not on. 

###Extra

- Go to the "Credits", theres a surprise for you (You may like it!)

We recommend you to post bugs and new suggestions at http://nzpotable.forumotion.com !
--