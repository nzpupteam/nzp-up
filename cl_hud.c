/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// cl_hud.c -- status bar code

#include "quakedef.h"
#include <pspgu.h>



qpic_t		*sb_round[5];
qpic_t		*sb_round_num[10];
qpic_t		*sb_moneyback;
qpic_t		*instapic;
qpic_t		*x2pic;
qpic_t 		*revivepic;
qpic_t		*jugpic;
qpic_t		*floppic;
qpic_t		*staminpic;
qpic_t		*doublepic;
qpic_t		*speedpic;
qpic_t		*fragpic;
qpic_t		*bettypic;
qpic_t		*deadpic;

qpic_t 		*b_circle;
qpic_t 		*b_square;
qpic_t 		*b_cross;
qpic_t 		*b_triangle;
qpic_t 		*b_left;
qpic_t 		*b_right;
qpic_t 		*b_up;
qpic_t 		*b_down;
qpic_t 		*b_lt;
qpic_t 		*b_rt;
qpic_t 		*b_start;
qpic_t 		*b_select;
qpic_t 		*b_home;

qpic_t      *fx_blood_lu;
qpic_t      *fx_blood_ru;
qpic_t      *fx_blood_ld;
qpic_t      *fx_blood_rd;

qboolean	sb_showscores;

int  x_value, y_value;

void M_DrawPic (int x, int y, qpic_t *pic);

double HUD_Change_time;//hide hud when not chagned

extern cvar_t waypoint_mode;
extern cvar_t scr_drawmagtext;


int old_points;
int current_points;
int point_change_interval;
int point_change_interval_neg;
int alphabling = 0;
float round_center_x;
float round_center_y;

typedef struct
{
	int points;
	int negative;
	float x;
	float y;
	float move_x;
	float move_y;
	double alive_time;
} point_change_t;

  double fade_screen_time;//used to hide black fade

point_change_t point_change[10];
/*
===============
HUD_Init
===============
*/
void HUD_Init (void)
{
	int		i;

	for (i=0 ; i<5 ; i++)
	{
		sb_round[i] = Draw_CachePic (va("gfx/hud/r%i",i + 1));
	}

	for (i=0 ; i<10 ; i++)
	{
		sb_round_num[i] = Draw_CachePic (va("gfx/hud/r_num%i",i));
	}

	sb_moneyback = Draw_CachePic ("gfx/hud/moneyback.lmp");
	instapic = Draw_CachePic ("gfx/hud/in_kill.lmp");
	x2pic = Draw_CachePic ("gfx/hud/2x.lmp");

	revivepic = Draw_CachePic ("gfx/hud/revive.lmp");
	jugpic = Draw_CachePic ("gfx/hud/jug.lmp");
	floppic = Draw_CachePic ("gfx/hud/flopper.lmp");
	staminpic = Draw_CachePic ("gfx/hud/stamin.lmp");
	doublepic = Draw_CachePic ("gfx/hud/double.lmp");
	speedpic = Draw_CachePic ("gfx/hud/speed.lmp");
	fragpic = Draw_CachePic ("gfx/hud/frag.lmp");
	bettypic = Draw_CachePic ("gfx/hud/betty.lmp");
	deadpic = Draw_CachePic ("gfx/hud/dead.lmp");

	b_circle = Draw_CachePic ("gfx/butticons/circle");
	b_square = Draw_CachePic ("gfx/butticons/square");
	b_cross = Draw_CachePic ("gfx/butticons/cross");
	b_triangle = Draw_CachePic ("gfx/butticons/triangle");
	b_left = Draw_CachePic ("gfx/butticons/left");
	b_right = Draw_CachePic ("gfx/butticons/right");
	b_up = Draw_CachePic ("gfx/butticons/up");
	b_down = Draw_CachePic ("gfx/butticons/down");
	b_lt = Draw_CachePic ("gfx/butticons/lt");
	b_rt = Draw_CachePic ("gfx/butticons/rt");
	b_start = Draw_CachePic ("gfx/butticons/start");
	b_select = Draw_CachePic ("gfx/butticons/select");
	b_home = Draw_CachePic ("gfx/butticons/home");


    fx_blood_lu = Draw_CachePic ("gfx/hud/blood");
    /*fx_blood_lu = Draw_CachePic ("gfx/hud/blood_tl");
    /fx_blood_ru = Draw_CachePic ("gfx/hud/blood_tr");
    fx_blood_ld = Draw_CachePic ("gfx/hud/blood_bl");
    fx_blood_rd = Draw_CachePic ("gfx/hud/blood_br");*/

	Achievement_Init();
}

void HUD_ParseWorldspawn (void);
/*
===============
HUD_NewMap
===============
*/
void HUD_NewMap (void)
{
	int i;
	alphabling = 0;

	for (i=0 ; i<10 ; i++)
	{
		point_change[i].points = 0;
		point_change[i].negative = 0;
		point_change[i].x = 0.0;
		point_change[i].y = 0.0;
		point_change[i].move_x = 0.0;
		point_change[i].move_y = 0.0;
		point_change[i].alive_time = 0.0;
	}

	old_points = 500;
	current_points = 500;
	point_change_interval = 0;
	point_change_interval_neg = 0;

	round_center_x = (vid.width - sb_round[0]->width) /2;
	round_center_y = (vid.height - sb_round[0]->height) /2;
	
	fade_screen_time = Sys_FloatTime() + 2.6;
	
	//HUD_ParseWorldspawn ();
}


/*
=============
HUD_itoa
=============
*/
int HUD_itoa (int num, char *buf)
{
	char	*str;
	int		pow10;
	int		dig;

	str = buf;

	if (num < 0)
	{
		*str++ = '-';
		num = -num;
	}

	for (pow10 = 10 ; num >= pow10 ; pow10 *= 10)
	;

	do
	{
		pow10 /= 10;
		dig = num/pow10;
		*str++ = '0'+dig;
		num -= dig*pow10;
	} while (pow10 != 1);

	*str = 0;

	return str-buf;
}

/*
==================
HUD_LoadLocString

FAIL -- ABORT MISSION!!!!
==================

void HUD_LoadLocString (char *location) //load the location string for the hud
{
	//default string for location if set to ''
	if (location[0] == '0')
	{
		Draw_CSS (4, vid.height/2 + 90, "default location", 0, 1.25);
		return;
	}else
	{
		Draw_CSS (4, vid.height/2 + 70, location, 0, 1.25); //draw location string
	}
    //strcpy(location_name, location);
}
*/

/*
=============
HUD_ParseWorldspawn

called at map load to find user defined HUD strings
=============
*/
void HUD_ParseWorldspawn (void)
{
	char key[128], value[4096];
	char *data;
	
	float alpha;
	
	alpha = 170/100*200;
  float modifier = ((cl.time * 0.2));
  if(modifier < -25)
    modifier = -25;
    
  if (modifier == -25)
    modifier = ((cl.time * -0.2));
        
  if (modifier > 0)
    alpha -= modifier;
  else
    alpha += modifier;
    /*if(alpha < 0.0)
	    return; */
	
	if (cl.stats[STAT_ROUNDCHANGE] == 1)//this is the rounds icon at the middle of the screen
	{
		data = COM_Parse(cl.worldmodel->entities);
		if (!data)
			return; // error
		if (com_token[0] != '{')
			return; // error
	
		while (1)
		{
			data = COM_Parse(data);
			if (!data)
				return; // error
			if (com_token[0] == '}')
				break; // end of worldspawn
			if (com_token[0] == '_')
				strcpy(key, com_token + 1);
			else
				strcpy(key, com_token);
			while (key[strlen(key)-1] == ' ') // remove trailing spaces
				key[strlen(key)-1] = 0;
			data = COM_Parse(data);
			if (!data)
				return; // error
			strcpy(value, com_token);

			if (!strcmp("location", key)) //search for the location key
			{
				Draw_AlphaCSS (4, vid.height/2 + 70, value, 0, 1.25, alpha); //draw call
			}
			else if (!strcmp("date", key))
			{
				Draw_AlphaCSS (4, vid.height/2 + 80, value, 0, 1.25, alpha);
			}
			else if (!strcmp("person", key))
			{
				Draw_AlphaCSS (4, vid.height/2 + 90, value, 0, 1.25, alpha);
			}
		}
		}else
		{
			return;
		}
}



//=============================================================================

int		pointsort[MAX_SCOREBOARD];

char	scoreboardtext[MAX_SCOREBOARD][20];
int		scoreboardtop[MAX_SCOREBOARD];
int		scoreboardbottom[MAX_SCOREBOARD];
int		scoreboardcount[MAX_SCOREBOARD];
int		scoreboardlines;

/*
===============
HUD_Sorpoints
===============
*/
void HUD_Sortpoints (void)
{
	int		i, j, k;

// sort by points
	scoreboardlines = 0;
	for (i=0 ; i<cl.maxclients ; i++)
	{
		if (cl.scores[i].name[0])
		{
			pointsort[scoreboardlines] = i;
			scoreboardlines++;
		}
	}

	for (i=0 ; i<scoreboardlines ; i++)
		for (j=0 ; j<scoreboardlines-1-i ; j++)
			if (cl.scores[pointsort[j]].points < cl.scores[pointsort[j+1]].points)
			{
				k = pointsort[j];
				pointsort[j] = pointsort[j+1];
				pointsort[j+1] = k;
			}
}

/*
===============
HUD_EndScreen
===============
*/
void HUD_EndScreen (void)
{
	scoreboard_t	*s;
	char			str[80];
	int				i, k, l;
	int				y, x, d;

	HUD_Sortpoints ();

	l = scoreboardlines;

	Draw_ColoredString ((vid.width - 9*8)/2, 40, "GAME OVER", 3);

	sprintf (str,"You survived %3i rounds", cl.stats[STAT_ROUNDS]);
	Draw_String ((vid.width - strlen (str)*8)/2, 52, str);

	sprintf (str,"Name           Kills     Points");
	x = (vid.width - strlen (str)*8)/2;

	Draw_String (x, 68, str);
	y = 0;
	for (i=0; i<l ; i++)
	{
		k = pointsort[i];
		s = &cl.scores[k];
		if (!s->name[0])
			continue;

		Draw_ColoredString (x, 88 + y, s->name, 2);

		d = strlen (va("%i",s->kills));
		Draw_ColoredString (x + (20 - d)*8, 88 + y, va("%i",s->kills), 2);

		d = strlen (va("%i",s->points));
		Draw_ColoredString (x + (31 - d)*8, 88 + y, va("%i",s->points), 2);
		y += 10;
	}
}



   

//=============================================================================

  //=============================================================================//
 //===============================DRAW FUNCTIONS================================//
//=============================================================================//

/*
==================
HUD_Points

==================
*/


void HUD_Parse_Point_Change (int points, int negative, int x_start, int y_start)
{
	int i, f;
	char str[10];
	i=9;
	while (i>0)
	{
		point_change[i] = point_change[i - 1];
		i--;
	}

	point_change[i].points = points;
	point_change[i].negative = negative;

	f = HUD_itoa (points, str);
	point_change[i].x = x_start - 10.0 - 8.0*f;
	point_change[i].y = y_start;
	point_change[i].move_x = 1.0;
	point_change[i].move_y = ((rand ()&0x7fff) / ((float)0x7fff)) - 0.5;

	point_change[i].alive_time = Sys_FloatTime() + 0.4;


}

void HUD_Points (void)
{
	int				i, k, l;
	int				x, y, f, xplus;
	scoreboard_t	*s;
	char str[12];

// scores
	HUD_Sortpoints ();

// draw the text
	l = scoreboardlines;


    x = vid.width - sb_moneyback->width;
    y = vid.height - 16 - fragpic->height - 4 - 16 - sb_moneyback->height;
	for (i=0 ; i<l ; i++)
	{
		k = pointsort[i];
		s = &cl.scores[k];
		if (!s->name[0])
			continue;

	// draw background

	// draw number
		f = s->points;
		if (f > current_points)
		{
			point_change_interval_neg = 0;
			if (!point_change_interval)
			{
				point_change_interval = (int)(f - old_points)/55;;
			}
			current_points = old_points + point_change_interval;
			if (f < current_points)
			{
				current_points = f;
				point_change_interval = 0;
			}
		}
		else if (f < current_points)
		{
			point_change_interval = 0;
			if (!point_change_interval_neg)
			{
				point_change_interval_neg = (int)(old_points - f)/55;
			}
			current_points = old_points - point_change_interval_neg;
			if (f > current_points)
			{
				current_points = f;
				point_change_interval_neg = 0;
			}
		}
		Draw_Pic (x, y, sb_moneyback);
		xplus = HUD_itoa (f, str);
		Draw_String (vid.width - (xplus*8) - 16, y + 3, va("%i", current_points));

		if (old_points != f)
		{
			if (f > old_points)
				HUD_Parse_Point_Change(f - old_points, 0, vid.width - (xplus*8) - 16, y + 3);
			else
				HUD_Parse_Point_Change(old_points - f, 1, vid.width - (xplus*8) - 16, y + 3);

			old_points = f;
		}



		y += 10;
	}
}


/*
==================
HUD_Point_Change

==================
*/
void HUD_Point_Change (void)
{
	int	i;

	for (i=0 ; i<10 ; i++)
	{
		if (point_change[i].points)
		{
			if (point_change[i].negative)
				Draw_ColoredString (point_change[i].x, point_change[i].y, va ("-%i", point_change[i].points),3);
			else
				Draw_ColoredString (point_change[i].x, point_change[i].y, va ("+%i", point_change[i].points),1);
			point_change[i].y = point_change[i].y + point_change[i].move_y;
			point_change[i].x = point_change[i].x - point_change[i].move_x;
			if (point_change[i].alive_time && point_change[i].alive_time < Sys_FloatTime())
			{
				point_change[i].points = 0;
				point_change[i].negative = 0;
				point_change[i].x = 0.0;
				point_change[i].y = 0.0;
				point_change[i].move_x = 0.0;
				point_change[i].move_y = 0.0;
				point_change[i].alive_time = 0.0;
			}
		}
	}
}


/*
==================
HUD_Blood

==================
*/
void HUD_Blood (void)
{
    float alpha;
	//blubswillrule:
	//this function scales linearly from health = 0 to health = 100
	//alpha = (100.0 - (float)cl.stats[STAT_HEALTH])/100*255;
	//but we want the picture to be fully visible at health = 20, so use this function instead
	alpha = (100.0 - ((1.25 * (float) cl.stats[STAT_HEALTH]) - 25))/100*255;
	
    if (alpha <= 0.0)
        return;
    
    float modifier = (sin(cl.time * 10) * 20) - 20;//always negative
    if(modifier < -35.0)
	modifier = -35.0;
    
    alpha += modifier;
    
    if(alpha < 0.0)
	    return;
    float color = 255.0 + modifier;
    
    Draw_ColorPic(0,0,fx_blood_lu,color,color,color,alpha);
    //Draw_ColorPic (0, 0, fx_blood_lu, 82, 6, 6, alpha);
    /*Draw_ColorPic (0, vid.height - fx_blood_ru->height, fx_blood_ld, 82, 6, 6, alpha);
    Draw_ColorPic (vid.width - fx_blood_ru->width, 0, fx_blood_ru, 82, 6, 6, alpha);
    Draw_ColorPic (vid.width - fx_blood_ru->width, vid.height - fx_blood_ru->height, fx_blood_rd, 82, 6, 6, alpha);*/
}

/*
===============
HUD_Rounds
===============
*/

float 	color_shift[3];
float 	color_shift_end[3];
float 	color_shift_steps[3];
int		color_shift_init;
int 	blinking;

void HUD_Rounds (void)
{
	int i, x_offset, icon_num, savex;
	int num[3];
	x_offset = 0;
	savex = 0;
	float alpha;
	
	alpha = 170/100*200;
  float modifier = ((cl.time * 0.2));
  if(modifier < -25)
    modifier = -25;
    
  if (modifier == -25)
    modifier = ((cl.time * -0.2));
        
  if (modifier > 0)
    alpha -= modifier;
  else
    alpha += modifier;
    /*if(alpha < 0.0)
	    return; */

	if (cl.stats[STAT_ROUNDCHANGE] == 1)//this is the rounds icon at the middle of the screen
	{
		Draw_ColorPic ((vid.width - sb_round[0]->width) /2, (vid.height - sb_round[0]->height) /2, sb_round[0], 107, 1, 0, alphabling);
		
		Draw_AlphaCSS (4, vid.height/2 + 60, "'Nazi Zombies'", 0, 1.25, alpha);
		
		//scatterbox -- bye bye :')
		
		//Draw_CSS (4, vid.height/2 + 70, "Mabuse's House, Germany", 0, 1.25);
		//Draw_CSS (4, vid.height/2 + 80, "November 3rd 1943", 0, 1.25);
		//Draw_CSS (4, vid.height/2 + 90, "Pvt. Blubs (M.I.A)", 0, 1.25);
		
		Draw_AlphaCSS (vid.width/2-strlen("Round")*8, round_center_y - 20, "Round", 3, 2, alpha);		//I got it lel
		

		alphabling = alphabling + 15;

		if (alphabling < 0)
			alphabling = 0;
		else if (alphabling > 255)
			alphabling = 255;
	}
	else if (cl.stats[STAT_ROUNDCHANGE] == 2)//this is the rounds icon moving from middle
	{
		Draw_ColorPic (round_center_x, round_center_y, sb_round[0], 107, 1, 0, 255);
		round_center_x = round_center_x - (229/108)*2 - 0.2;
		round_center_y = round_center_y + 2;
		if (round_center_x <= 5)
			round_center_x = 5;
		if (round_center_y >= 220)
			round_center_y = 220;
	}
	else if (cl.stats[STAT_ROUNDCHANGE] == 3)//shift to white
	{
		if (!color_shift_init)
		{
			color_shift[0] = 107;
			color_shift[1] = 1;
			color_shift[2] = 0;
			for (i = 0; i < 3; i++)
			{
				color_shift_end[i] = 255;
				color_shift_steps[i] = (color_shift_end[i] - color_shift[i])/60;
			}
			color_shift_init = 1;
		}
		for (i = 0; i < 3; i++)
		{
			if (color_shift[i] < color_shift_end[i])
				color_shift[i] = color_shift[i] + color_shift_steps[i];

			if (color_shift[i] >= color_shift_end[i])
				color_shift[i] = color_shift_end[i];
		}
		if (cl.stats[STAT_ROUNDS] > 0 && cl.stats[STAT_ROUNDS] < 11)
		{

			for (i = 0; i < cl.stats[STAT_ROUNDS]; i++)
			{
				if (i == 4)
				{
					Draw_ColorPic (5, vid.height - sb_round[4]->height - 4, sb_round[4], (int)color_shift[0], (int)color_shift[1], (int)color_shift[2], 255);
					savex = x_offset + 10;
					x_offset = x_offset + 10;
					continue;
				}
				if (i == 9)
				{
					Draw_ColorPic (5 + savex, vid.height - sb_round[4]->height - 4, sb_round[4], (int)color_shift[0], (int)color_shift[1], (int)color_shift[2], 255);
					continue;
				}
				if (i > 4)
					icon_num = i - 5;
				else
					icon_num = i;

				Draw_ColorPic (5 + x_offset, vid.height - sb_round[icon_num]->height - 4, sb_round[icon_num], (int)color_shift[0], (int)color_shift[1], (int)color_shift[2], 255);

				x_offset = x_offset + sb_round[icon_num]->width + 3;
			}
		}
		else
		{
			if (cl.stats[STAT_ROUNDS] >= 100)
			{
				num[2] = (int)(cl.stats[STAT_ROUNDS]/100);
				Draw_ColorPic (2 + x_offset, vid.height - sb_round_num[num[2]]->height - 4, sb_round_num[num[2]], (int)color_shift[0], (int)color_shift[1], (int)color_shift[2], 255);
				x_offset = x_offset + sb_round_num[num[2]]->width - 8;
			}
			else
				num[2] = 0;
			if (cl.stats[STAT_ROUNDS] >= 10)
			{
				num[1] = (int)((cl.stats[STAT_ROUNDS] - num[2]*100)/10);
				Draw_ColorPic (2 + x_offset, vid.height - sb_round_num[num[1]]->height - 4, sb_round_num[num[1]], (int)color_shift[0], (int)color_shift[1], (int)color_shift[2], 255);
				x_offset = x_offset + sb_round_num[num[1]]->width - 8;
			}
			else
				num[1] = 0;

			num[0] = cl.stats[STAT_ROUNDS] - num[2]*100 - num[1]*10;
			Draw_ColorPic (2 + x_offset, vid.height - sb_round_num[num[0]]->height - 4, sb_round_num[num[0]], (int)color_shift[0], (int)color_shift[1], (int)color_shift[2], 255);
			x_offset = x_offset + sb_round_num[num[0]]->width - 8;
		}
	}
	else if (cl.stats[STAT_ROUNDCHANGE] == 4)//blink white
	{
		blinking = ((int)(realtime*1000)&510) - 255;
		blinking = abs(blinking);
		if (cl.stats[STAT_ROUNDS] > 0 && cl.stats[STAT_ROUNDS] < 11)
		{
			for (i = 0; i < cl.stats[STAT_ROUNDS]; i++)
			{
				if (i == 4)
				{
					Draw_ColorPic (5, vid.height - sb_round[4]->height - 4, sb_round[4], 255, 255, 255, blinking);
					savex = x_offset + 10;
					x_offset = x_offset + 10;
					continue;
				}
				if (i == 9)
				{
					Draw_ColorPic (5 + savex, vid.height - sb_round[4]->height - 4, sb_round[4], 255, 255, 255, blinking);
					continue;
				}
				if (i > 4)
					icon_num = i - 5;
				else
					icon_num = i;

				Draw_ColorPic (5 + x_offset, vid.height - sb_round[icon_num]->height - 4, sb_round[icon_num], 255, 255, 255, blinking);

				x_offset = x_offset + sb_round[icon_num]->width + 3;
			}
		}
		else
		{
			if (cl.stats[STAT_ROUNDS] >= 100)
			{
				num[2] = (int)(cl.stats[STAT_ROUNDS]/100);
				Draw_ColorPic (2 + x_offset, vid.height - sb_round_num[num[2]]->height - 4, sb_round_num[num[2]], 255, 255, 255, blinking);
				x_offset = x_offset + sb_round_num[num[2]]->width - 8;
			}
			else
				num[2] = 0;
			if (cl.stats[STAT_ROUNDS] >= 10)
			{
				num[1] = (int)((cl.stats[STAT_ROUNDS] - num[2]*100)/10);
				Draw_ColorPic (2 + x_offset, vid.height - sb_round_num[num[1]]->height - 4, sb_round_num[num[1]], 255, 255, 255, blinking);
				x_offset = x_offset + sb_round_num[num[1]]->width - 8;
			}
			else
				num[1] = 0;

			num[0] = cl.stats[STAT_ROUNDS] - num[2]*100 - num[1]*10;
			Draw_ColorPic (2 + x_offset, vid.height - sb_round_num[num[0]]->height - 4, sb_round_num[num[0]], 255, 255, 255, blinking);
			x_offset = x_offset + sb_round_num[num[0]]->width - 8;
		}
	}
	else if (cl.stats[STAT_ROUNDCHANGE] == 5)//blink white
	{
		if (blinking > 0)
			blinking = blinking - 10;
		if (blinking < 0)
			blinking = 0;
		if (cl.stats[STAT_ROUNDS] > 0 && cl.stats[STAT_ROUNDS] < 11)
		{
			for (i = 0; i < cl.stats[STAT_ROUNDS]; i++)
			{
				if (i == 4)
				{
					Draw_ColorPic (5, vid.height - sb_round[4]->height - 4, sb_round[4], 255, 255, 255, blinking);
					savex = x_offset + 10;
					x_offset = x_offset + 10;
					continue;
				}
				if (i == 9)
				{
					Draw_ColorPic (5 + savex, vid.height - sb_round[4]->height - 4, sb_round[4], 255, 255, 255, blinking);
					continue;
				}
				if (i > 4)
					icon_num = i - 5;
				else
					icon_num = i;

				Draw_ColorPic (5 + x_offset, vid.height - sb_round[icon_num]->height - 4, sb_round[icon_num], 255, 255, 255, blinking);

				x_offset = x_offset + sb_round[icon_num]->width + 3;
			}
		}
		else
		{
			if (cl.stats[STAT_ROUNDS] >= 100)
			{
				num[2] = (int)(cl.stats[STAT_ROUNDS]/100);
				Draw_ColorPic (2 + x_offset, vid.height - sb_round_num[num[2]]->height - 4, sb_round_num[num[2]], 255, 255, 255, blinking);
				x_offset = x_offset + sb_round_num[num[2]]->width - 8;
			}
			else
				num[2] = 0;
			if (cl.stats[STAT_ROUNDS] >= 10)
			{
				num[1] = (int)((cl.stats[STAT_ROUNDS] - num[2]*100)/10);
				Draw_ColorPic (2 + x_offset, vid.height - sb_round_num[num[1]]->height - 4, sb_round_num[num[1]], 255, 255, 255, blinking);
				x_offset = x_offset + sb_round_num[num[1]]->width - 8;
			}
			else
				num[1] = 0;

			num[0] = cl.stats[STAT_ROUNDS] - num[2]*100 - num[1]*10;
			Draw_ColorPic (2 + x_offset, vid.height - sb_round_num[num[0]]->height - 4, sb_round_num[num[0]], 255, 255, 255, blinking);
			x_offset = x_offset + sb_round_num[num[0]]->width - 8;
		}
	}
	else if (cl.stats[STAT_ROUNDCHANGE] == 6)//blink white while fading back
	{
		color_shift_init = 0;
		blinking = ((int)(realtime*1000)&510) - 255;
		blinking = abs(blinking);
		if (cl.stats[STAT_ROUNDS] > 0 && cl.stats[STAT_ROUNDS] < 11)
		{
			for (i = 0; i < cl.stats[STAT_ROUNDS]; i++)
			{
				if (i == 4)
				{
					Draw_ColorPic (5, vid.height - sb_round[4]->height - 4, sb_round[4], 255, 255, 255, blinking);
					savex = x_offset + 10;
					x_offset = x_offset + 10;
					continue;
				}
				if (i == 9)
				{
					Draw_ColorPic (5 + savex, vid.height - sb_round[4]->height - 4, sb_round[4], 255, 255, 255, blinking);
					continue;
				}
				if (i > 4)
					icon_num = i - 5;
				else
					icon_num = i;

				Draw_ColorPic (5 + x_offset, vid.height - sb_round[icon_num]->height - 4, sb_round[icon_num], 255, 255, 255, blinking);

				x_offset = x_offset + sb_round[icon_num]->width + 3;
			}
		}
		else
		{
			if (cl.stats[STAT_ROUNDS] >= 100)
			{
				num[2] = (int)(cl.stats[STAT_ROUNDS]/100);
				Draw_ColorPic (2 + x_offset, vid.height - sb_round_num[num[2]]->height - 4, sb_round_num[num[2]], 255, 255, 255, blinking);
				x_offset = x_offset + sb_round_num[num[2]]->width - 8;
			}
			else
				num[2] = 0;
			if (cl.stats[STAT_ROUNDS] >= 10)
			{
				num[1] = (int)((cl.stats[STAT_ROUNDS] - num[2]*100)/10);
				Draw_ColorPic (2 + x_offset, vid.height - sb_round_num[num[1]]->height - 4, sb_round_num[num[1]], 255, 255, 255, blinking);
				x_offset = x_offset + sb_round_num[num[1]]->width - 8;
			}
			else
				num[1] = 0;

			num[0] = cl.stats[STAT_ROUNDS] - num[2]*100 - num[1]*10;
			Draw_ColorPic (2 + x_offset, vid.height - sb_round_num[num[0]]->height - 4, sb_round_num[num[0]], 255, 255, 255, blinking);
			x_offset = x_offset + sb_round_num[num[0]]->width - 8;
		}
	}
	else if (cl.stats[STAT_ROUNDCHANGE] == 7)//blink white while fading back
	{
		if (!color_shift_init)
		{
			color_shift_end[0] = 107;
			color_shift_end[1] = 1;
			color_shift_end[2] = 0;
			for (i = 0; i < 3; i++)
			{
				color_shift[i] = 255;
				color_shift_steps[i] = (color_shift[i] - color_shift_end[i])/60;
			}
			color_shift_init = 1;
		}
		for (i = 0; i < 3; i++)
		{
			if (color_shift[i] > color_shift_end[i])
				color_shift[i] = color_shift[i] - color_shift_steps[i];

			if (color_shift[i] < color_shift_end[i])
				color_shift[i] = color_shift_end[i];
		}
		if (cl.stats[STAT_ROUNDS] > 0 && cl.stats[STAT_ROUNDS] < 11)
		{
			for (i = 0; i < cl.stats[STAT_ROUNDS]; i++)
			{
				if (i == 4)
				{
					Draw_ColorPic (5, vid.height - sb_round[4]->height - 4, sb_round[4], (int)color_shift[0], (int)color_shift[1], (int)color_shift[2], 255);
					savex = x_offset + 10;
					x_offset = x_offset + 10;
					continue;
				}
				if (i == 9)
				{
					Draw_ColorPic (5 + savex, vid.height - sb_round[4]->height - 4, sb_round[4], (int)color_shift[0], (int)color_shift[1], (int)color_shift[2], 255);
					continue;
				}
				if (i > 4)
					icon_num = i - 5;
				else
					icon_num = i;

				Draw_ColorPic (5 + x_offset, vid.height - sb_round[icon_num]->height - 4, sb_round[icon_num], (int)color_shift[0], (int)color_shift[1], (int)color_shift[2], 255);

				x_offset = x_offset + sb_round[icon_num]->width + 3;
			}
		}
		else
		{
			if (cl.stats[STAT_ROUNDS] >= 100)
			{
				num[2] = (int)(cl.stats[STAT_ROUNDS]/100);
				Draw_ColorPic (2 + x_offset, vid.height - sb_round_num[num[2]]->height - 4, sb_round_num[num[2]], (int)color_shift[0], (int)color_shift[1], (int)color_shift[2], 255);
				x_offset = x_offset + sb_round_num[num[2]]->width - 8;
			}
			else
				num[2] = 0;
			if (cl.stats[STAT_ROUNDS] >= 10)
			{
				num[1] = (int)((cl.stats[STAT_ROUNDS] - num[2]*100)/10);
				Draw_ColorPic (2 + x_offset, vid.height - sb_round_num[num[1]]->height - 4, sb_round_num[num[1]], (int)color_shift[0], (int)color_shift[1], (int)color_shift[2], 255);
				x_offset = x_offset + sb_round_num[num[1]]->width - 8;
			}
			else
				num[1] = 0;

			num[0] = cl.stats[STAT_ROUNDS] - num[2]*100 - num[1]*10;
			Draw_ColorPic (2 + x_offset, vid.height - sb_round_num[num[0]]->height - 4, sb_round_num[num[0]], (int)color_shift[0], (int)color_shift[1], (int)color_shift[2], 255);
			x_offset = x_offset + sb_round_num[num[0]]->width - 8;
		}
	}
	else
	{
		color_shift[0] = 107;
		color_shift[1] = 1;
		color_shift[2] = 0;
		color_shift_init = 0;
		alphabling = 0;
		if (cl.stats[STAT_ROUNDS] > 0 && cl.stats[STAT_ROUNDS] < 11)
		{
			for (i = 0; i < cl.stats[STAT_ROUNDS]; i++)
			{
				if (i == 4)
				{
					Draw_ColorPic (5, vid.height - sb_round[4]->height - 4, sb_round[4], 107, 1, 0, 255);
					savex = x_offset + 10;
					x_offset = x_offset + 10;
					continue;
				}
				if (i == 9)
				{
					Draw_ColorPic (5 + savex, vid.height - sb_round[4]->height - 4, sb_round[4], 107, 1, 0, 255);
					continue;
				}
				if (i > 4)
					icon_num = i - 5;
				else
					icon_num = i;

				Draw_ColorPic (5 + x_offset, vid.height - sb_round[icon_num]->height - 4, sb_round[icon_num], 107, 1, 0, 255);

				x_offset = x_offset + sb_round[icon_num]->width + 3;
			}
		}
		else
		{
			if (cl.stats[STAT_ROUNDS] >= 100)
			{
				num[2] = (int)(cl.stats[STAT_ROUNDS]/100);
				Draw_ColorPic (2 + x_offset, vid.height - sb_round_num[num[2]]->height - 4, sb_round_num[num[2]], 107, 1, 0, 255);
				x_offset = x_offset + sb_round_num[num[2]]->width - 8;
			}
			else
				num[2] = 0;
			if (cl.stats[STAT_ROUNDS] >= 10)
			{
				num[1] = (int)((cl.stats[STAT_ROUNDS] - num[2]*100)/10);
				Draw_ColorPic (2 + x_offset, vid.height - sb_round_num[num[1]]->height - 4, sb_round_num[num[1]], 107, 1, 0, 255);
				x_offset = x_offset + sb_round_num[num[1]]->width - 8;
			}
			else
				num[1] = 0;

			num[0] = cl.stats[STAT_ROUNDS] - num[2]*100 - num[1]*10;
			
			if(cl.stats[STAT_ROUNDS] == 0)
				return;
			
			Draw_ColorPic (2 + x_offset, vid.height - sb_round_num[num[0]]->height - 4, sb_round_num[num[0]], 107, 1, 0, 255);
			x_offset = x_offset + sb_round_num[num[0]]->width - 8;
		}
	}
}

/*
===============
HUD_Perks
===============
*/
#define 	P_JUG		1
#define 	P_DOUBLE	2
#define 	P_SPEED		4
#define 	P_REVIVE	8
#define 	P_FLOP		16
#define 	P_STAMIN	32
#define 	P_DEAD	  64

int perk_order[9];
int current_perk_order;

void HUD_Perks (void)
{
	int i;
	int y;
	y = vid.height - sb_round[1]->height - jugpic->height -2;

	for (i = 0; i < 8; i++)
	{
		if (perk_order[i])
		{
			if (perk_order[i] == P_JUG)
			{
				Draw_StretchPic (2, y, jugpic, 20, 20);
				y = y - 22;
			}
			else if (perk_order[i] == P_DOUBLE)
			{
				Draw_StretchPic (2, y, doublepic, 20, 20);
				y = y - 22;
			}
			else if (perk_order[i] == P_SPEED)
			{
				Draw_StretchPic (2, y, speedpic, 20, 20);
				y = y - 22;
			}
			else if (perk_order[i] == P_REVIVE)
			{
				Draw_StretchPic (2, y, revivepic, 20, 20);
				y = y - 22;
			}
			else if (perk_order[i] == P_FLOP)
			{
				Draw_StretchPic (2, y, floppic, 20, 20);
				y = y - 22;
			}
			else if (perk_order[i] == P_STAMIN)
			{
				Draw_StretchPic (2, y, staminpic, 20, 20);
				y = y - 22;
			}
			else if (perk_order[i] == P_DEAD)
			{
				Draw_StretchPic (2, y, deadpic, 20, 20);
				y = y - 22;
			}
		}
	}
}

/*
===============
HUD_Powerups
===============
*/
void HUD_Powerups (void)
{

	if(cl.stats[STAT_X2])
		Draw_StretchPic ((vid.width/2) - 30,0, x2pic, 28, 28);
	if(cl.stats[STAT_INSTA])
		Draw_StretchPic ((vid.width/2) + 2,0, instapic, 28, 28);
}

/*
===============
HUD_ProgressBar
===============
*/
void HUD_ProgressBar (void)
{
	float progressbar;

	if (cl.progress_bar)
	{
		progressbar = 100 - ((cl.progress_bar-sv.time)*10);
		if (progressbar >= 100)
			progressbar = 100;
		Draw_FillByColor  ((vid.width)/2 - 51, vid.height*0.75 - 1, 102, 5, GU_RGBA(0, 0, 0,100));
		Draw_FillByColor ((vid.width)/2 - 50, vid.height*0.75, progressbar, 3, GU_RGBA(255, 255, 255,100));

		Draw_String ((vid.width - (88))/2, vid.height*0.75 + 10, "Reviving...");
	}
}

/*
===============
HUD_Achievement

Achievements based on code by Arkage
===============
*/


int		achievement; // the background image
int		achievement_unlocked;
char		achievement_text[MAX_QPATH];
double		achievement_time;
float smallsec;
int ach_pic;
void HUD_Achievement (void)
{
	if (achievement_unlocked == 1)
	{
		smallsec = smallsec + 0.7;
		if (smallsec >= 55)
			smallsec = 55;
		//Background image
		//Sbar_DrawPic (176, 5, achievement);
		// The achievement text
		Draw_AlphaPic (30, smallsec - 50, achievement_list[ach_pic].img, 0.7f);
	}

	// Reset the achievement
	if (Sys_FloatTime() >= achievement_time)
	{
		achievement_unlocked = 0;
	}
}


void HUD_Parse_Achievement (int ach)
{
    if (achievement_list[ach].unlocked)
        return;

	achievement_unlocked = 1;
	smallsec = 0;
	achievement_time = Sys_FloatTime() + 10;
	ach_pic = ach;
	achievement_list[ach].unlocked = 1;
	Save_Achivements();
}

/*
===============
HUD_Ammo
===============
*/

int GetLowAmmo(int weapon, int type) // determine what ammo value you should have that turns shells red
{
	switch (weapon)
	{
		case W_COLT: if (type) return 2; else return 16; 
    case W_MUS_SALLY: if (type) return 2; else return 16;
		case W_KAR: if (type) return 1; else return 10;
		case W_KAR_SCOPE: if (type) return 1; else return 10;
		case W_M1A1: if (type) return 4; else return 24;
		case W_SAWNOFF: if (type) return 1; else return 12;
		case W_DB: if (type) return 1; else return 12;
		case W_THOMPSON: if (type) return 6; else return 40;
		case W_BAR: if (type) return 6; else return 28;
		case W_BIATCH: if (type) return 2; else return 16;
		case W_FG: if (type) return 8; else return 32;
		case W_357: if (type) return 2; else return 12; 
		case W_BROWNING: if (type) return 25; else return 125;
		case W_GEWEHR: if (type) return 3; else return 20;
		case W_M1: if (type) return 2; else return 24;
		case W_MP40: if (type) return 8; else return 64; 
		case W_MG: if (type) return 25; else return 125;
		case W_PANZER: if (type) return 0; else return 4;
		case W_PPSH: if (type) return 25; else return 71;
		case W_PTRS: if (type) return 2; else return 12; 
		case W_RAY: if (type) return 5; else return 40;
		case W_STG: if (type) return 12; else return 60;
		case W_TRENCH: if (type) return 2; else return 12;
		case W_TYPE: if (type) return 12; else return 60;
		case W_COMPRESSOR: if (type) return 3; else return 36;
		case W_M1000: if (type) return 3; else return 36;
		case W_GIBS: if (type) return 8; else return 80;
		case W_SAMURAI: if (type) return 12; else return 60;
		case W_AFTERBURNER: if (type) return 12; else return 64;
		case W_SPATZ: if (type) return 12; else return 120;
		case W_SNUFF: if (type) return 1; else return 14;
		case W_BORE: if (type) return 1; else return 14;
		case W_IMPELLER: if (type) return 12; else return 128;
		case W_BARRACUDA: if (type) return 25; else return 125;
		case W_ACCELERATOR: if (type) return 25; else return 125;
		case W_GUT: if (type) return 3; else return 15;
		case W_REAPER: if (type) return 23; else return 115;
		case W_HEADCRACKER: if (type) return 2; else return 16;
		case W_LONGINUS: if (type) return 1; else return 12;
		case W_PENETRATOR: if (type) return 2; else return 16;
		case W_WIDOW: if (type) return 6; else return 60;
		case W_PORTER: if (type) return 8; else return 40;
		case W_ARMAGEDDON: if (type) return 2; else return 15;
		case W_WIDDER: if (type) return 4; else return 30;
		case W_KILLU: if (type) return 2; else return 20;
		case W_TESLA: if (type) return 1; else return 6;
		case W_M14: if (type) return 2; else return 24;
		case 0: if (type) return -1; else return -1;
		default: return 0;
	}
}

int getWeaponMag(int weapon)
{

	switch (weapon)
	{
		case W_COLT:
			return 8;
			break;
    case W_MUS_SALLY:
      return 6;
      break;
		case W_BIATCH:
			return 6;
			break;
		case W_KAR:
			return 5;
			break;
		case W_THOMPSON:
			return 20;
			break;
		case W_357:
			return 6;
			break;
		case W_KILLU:
			return 6;
			break;
		case W_BAR:
			return 20;
			break;
		case W_BK:
			return 1;
			break;
		case W_BROWNING:
			return 125;
			break;
		case W_DB:
			return 2;
			break;
		case W_FG:
			return 32;
			break;
		case W_GEWEHR:
			return 10;
			break;
		case W_KAR_SCOPE:
			return 5;
			break;
		case W_M1:
			return 8;
			break;
		case W_M1A1:
			return 15;
			break;
		case W_MP40:
			return 32;
			break;
		case W_MG:
			return 125;
			break;
		case W_PANZER:
			return 1;
			break;
		case W_PPSH:
			return 71;
			break;
		case W_PTRS:
			return 5;
			break;
		case W_RAY:
			return 20;
			break;
		case W_SAWNOFF:
			return 2;
			break;
		case W_STG:
			return 30;
			break;
		case W_TRENCH:
			return 6;
			break;
		case W_TYPE:
			return 30;
			break;
		case W_M2:
			return 200;
			break;
		case W_COMPRESSOR:
      return 12;
      break;
    case W_M1000:
      return 12;
      break;
    case W_GIBS:
      return 40;
      break;
    case W_SAMURAI:
      return 60;
      break;
    case W_AFTERBURNER:
      return 64;
      break;
    case W_SPATZ:
      return 60;
      break;
    case W_SNUFF:
      return 2;
      break;
    case W_BORE:
      return 2;
      break;
    case W_IMPELLER:
      return 64;
      break;
    case W_BARRACUDA:
      return 125;
      break;
    case W_ACCELERATOR:
      return 125;
      break;
    case W_GUT:
      return 10;
      break;
    case W_REAPER:
      return 115;
      break;
    case W_HEADCRACKER:
      return 8;
      break;
    case W_LONGINUS:
      return 3;
      break;
    case W_PENETRATOR:
      return 8;
      break;
    case W_WIDOW:
      return 30;
      break;
    case W_PORTER:
      return 40;
      break;
    case W_ARMAGEDDON:
      return 8;
      break;
    case W_WIDDER:
      return 15;
      break;
    case W_MP5:
      return 30;
      break;
    case W_TESLA:
      return 3;
      break;
    case W_M14:
			return 8;
			break;
	}
	return 0;
}

int getAutoOffset(int weapon) //naievil -- for auto weapons only, determine offset for maglen <= 32
{

	switch (weapon)
	{
		case W_THOMPSON:
			return -24;
			break;
		case W_BAR:
			return -24;
			break;
		case W_BROWNING:
			return 186;
			break;
		case W_FG:
			return 0;
			break;
		case W_MP40:
			return 0;
			break;
		case W_MG:
			return 186;
			break;
		case W_PPSH:
			return 78;
			break;
		case W_RAY:
			return -24;
			break;
		case W_STG:
			return -4;
			break;
		case W_TYPE:
			return -4;
			break;
    case W_GIBS:
      return 16;
      break;
    case W_SAMURAI:
      return 56;
      break;
    case W_AFTERBURNER:
      return 64;
      break;
    case W_SPATZ:
      return 56;
      break;
    case W_IMPELLER:
      return 64;
      break;
    case W_BARRACUDA:
      return 186;
      break;
    case W_ACCELERATOR:
      return 186;
      break;
    case W_REAPER:
      return 166;
      break;
    case W_WIDOW:
      return -4;
      break;
    case W_PORTER:
      return 16;
      break;
    case W_MP5: //naievil -- I hate the mp5k so it will be intentionally glitched
      return 30;
      break;
	}
	return 0;
}

void HUD_Ammo (void)
{
  qpic_t *shell;
  int maglen;
  int x_offset;
  int x_dir;
  int backshellnum;
  qpic_t *emptyshell;
  int shelldiff; 
  float alpha;
	qpic_t *red_shell;
	qpic_t *back_shell;
	int autooffset;
	char str[12];
  int xplus;
	char *magstring;
	int x_factor;
	
	if (scr_drawmagtext.value)
	{
	if (GetLowAmmo(cl.stats[STAT_ACTIVEWEAPON], 1) >= cl.stats[STAT_CURRENTMAG])
		magstring = va ("&cF00%i",cl.stats[STAT_CURRENTMAG]);
	else
		magstring = va ("%i",cl.stats[STAT_CURRENTMAG]);

	xplus = HUD_itoa (cl.stats[STAT_CURRENTMAG], str);
	Draw_ColoredString (vid.width - 42 - (xplus*8), vid.height - 16, magstring, 0);
	}
	else
	{
	
	alpha = 170/100*255;
    
  float modifier = ((cl.time * 1.7));
  
  if(modifier < -25.0)
    modifier = -25.0;
	
  if (modifier == -25.0)
    modifier = ((cl.time * -1.7));
    
    
    alpha += modifier;
    
    if(alpha < 0.0)
	    return;    
  
  y_value = vid.height - 16;
  x_offset = 0;
  x_dir = 1;
  
  maglen = cl.stats[STAT_CURRENTMAG];
  backshellnum = getWeaponMag(cl.stats[STAT_ACTIVEWEAPON]);
  
  if (cl.stats[STAT_ACTIVEWEAPON] == 11 || cl.stats[STAT_ACTIVEWEAPON] == 19 || cl.stats[STAT_ACTIVEWEAPON] == 49 || cl.stats[STAT_ACTIVEWEAPON] == 51)  // snipers only
  {
    shell = Draw_CacheImg ("gfx/hud/sniper_shell");
    emptyshell = Draw_CacheImg ("gfx/hud/sniper_shell_empty");
    x_factor = 9;
    
   if (GetLowAmmo(cl.stats[STAT_ACTIVEWEAPON], 1) >= cl.stats[STAT_CURRENTMAG])
    shell = Draw_CacheImg ("gfx/hud/sniper_shell_low");
    
    red_shell = Draw_CacheImg ("gfx/hud/sniper_shell_low");    
  }
  else if (cl.stats[STAT_ACTIVEWEAPON] == 21 || cl.stats[STAT_ACTIVEWEAPON] == 8 || cl.stats[STAT_ACTIVEWEAPON] == 23 || 
           cl.stats[STAT_ACTIVEWEAPON] == 42 || cl.stats[STAT_ACTIVEWEAPON] == 47 || cl.stats[STAT_ACTIVEWEAPON] == 43)  // shotguns
  {
    shell = Draw_CacheImg ("gfx/hud/shotgun_shell");
    emptyshell = Draw_CacheImg ("gfx/hud/shotgun_shell_empty");
    x_factor = 5;
    
   if (GetLowAmmo(cl.stats[STAT_ACTIVEWEAPON], 1) >= cl.stats[STAT_CURRENTMAG])
    shell = Draw_CacheImg ("gfx/hud/shotgun_shell_low");
    
    red_shell = Draw_CacheImg ("gfx/hud/shotgun_shell_low");    
  }
  else
  {
    shell = Draw_CacheImg ("gfx/hud/shell");
    emptyshell = Draw_CacheImg ("gfx/hud/empty_shell");
    x_factor = 1;
    
   if (GetLowAmmo(cl.stats[STAT_ACTIVEWEAPON], 1) >= cl.stats[STAT_CURRENTMAG])
    shell = Draw_CacheImg ("gfx/hud/low_shell");
    
      red_shell = Draw_CacheImg ("gfx/hud/red_shell");
  }
 
  back_shell = Draw_CacheImg ("gfx/hud/back_shell");

  shelldiff = backshellnum - maglen;
  
  autooffset = getAutoOffset(cl.stats[STAT_ACTIVEWEAPON]);

  while (maglen > 0)
   {
    if (cl.stats[STAT_ACTIVEWEAPON] == 0 || cl.stats[STAT_ACTIVEWEAPON] == 1 || cl.stats[STAT_ACTIVEWEAPON] == 2 || 
        cl.stats[STAT_ACTIVEWEAPON] == 4 || cl.stats[STAT_ACTIVEWEAPON] == 8 || cl.stats[STAT_ACTIVEWEAPON] == 10 || 
        cl.stats[STAT_ACTIVEWEAPON] == 11 || cl.stats[STAT_ACTIVEWEAPON] == 12 || cl.stats[STAT_ACTIVEWEAPON] == 13 || 
        cl.stats[STAT_ACTIVEWEAPON] == 17 || cl.stats[STAT_ACTIVEWEAPON] == 19 || cl.stats[STAT_ACTIVEWEAPON] == 21 || 
        cl.stats[STAT_ACTIVEWEAPON] == 23 || cl.stats[STAT_ACTIVEWEAPON] == 28 || cl.stats[STAT_ACTIVEWEAPON] == 29 || 
        cl.stats[STAT_ACTIVEWEAPON] == 30 || cl.stats[STAT_ACTIVEWEAPON] == 31 || cl.stats[STAT_ACTIVEWEAPON] == 34 || 
        cl.stats[STAT_ACTIVEWEAPON] == 36 || cl.stats[STAT_ACTIVEWEAPON] == 42 || cl.stats[STAT_ACTIVEWEAPON] == 43 || 
        cl.stats[STAT_ACTIVEWEAPON] == 47 || cl.stats[STAT_ACTIVEWEAPON] == 49 || cl.stats[STAT_ACTIVEWEAPON] == 50 || 
        cl.stats[STAT_ACTIVEWEAPON] == 51)
      x_offset = x_offset + 4; 
    else 
    {
      x_offset = x_offset + 3;
      if ((x_offset >= 100 && x_offset <= 200) || (x_offset > 298))    
        x_dir = -1;    
    }
    if (x_dir == 1)
    {
      if (cl.stats[STAT_ACTIVEWEAPON] == 11 || cl.stats[STAT_ACTIVEWEAPON] == 19 || 
          cl.stats[STAT_ACTIVEWEAPON] == 49 || cl.stats[STAT_ACTIVEWEAPON] == 51)  // snipers only
      {
        int secondrow;
        
        if (getWeaponMag(cl.stats[STAT_ACTIVEWEAPON]) > 5 && x_offset > 20)
        {
          y_value = vid.height - 26;
          secondrow = 180;
        }
        else
          secondrow = 0;

    
        if (GetLowAmmo(cl.stats[STAT_ACTIVEWEAPON], 1) >= cl.stats[STAT_CURRENTMAG])
          {Draw_Pic(vid.width - 44 - x_offset*x_factor + secondrow, y_value,emptyshell);
           Draw_Pic(vid.width - 44 - x_offset*x_factor + secondrow, y_value,red_shell);}
        else
          Draw_Pic (vid.width - 44 - x_offset*x_factor + secondrow, y_value, shell);
      }
      
      /* shotguns only here */ 
      
      else if (cl.stats[STAT_ACTIVEWEAPON] == 21 || cl.stats[STAT_ACTIVEWEAPON] == 8 || cl.stats[STAT_ACTIVEWEAPON] == 23 || 
           cl.stats[STAT_ACTIVEWEAPON] == 42 || cl.stats[STAT_ACTIVEWEAPON] == 47 || cl.stats[STAT_ACTIVEWEAPON] == 43)
      {
        if (GetLowAmmo(cl.stats[STAT_ACTIVEWEAPON], 1) >= cl.stats[STAT_CURRENTMAG])
          {Draw_Pic(vid.width - 44 - x_offset*x_factor, y_value,emptyshell);
           Draw_Pic(vid.width - 44 - x_offset*x_factor, y_value,red_shell);}
        else
          Draw_Pic (vid.width - 44 - x_offset*x_factor, y_value, shell);
      }
      
      /* end shotgun only */
      
      else
      {
        if (GetLowAmmo(cl.stats[STAT_ACTIVEWEAPON], 1) >= cl.stats[STAT_CURRENTMAG])
          {Draw_Pic(vid.width - 44 - x_offset*x_factor, y_value,emptyshell);
        
          if (cl.stats[STAT_ACTIVEWEAPON] == 11 || cl.stats[STAT_ACTIVEWEAPON] == 19 || 
              cl.stats[STAT_ACTIVEWEAPON] == 49 || cl.stats[STAT_ACTIVEWEAPON] == 51)
          {
            Draw_Pic(vid.width - 44 - x_offset*x_factor, y_value,red_shell);
          }
          else
          {Draw_AlphaPic(vid.width - 44 - x_offset*x_factor, y_value,red_shell,alpha);
          Draw_Pic (vid.width - 44 - x_offset*x_factor + 1, y_value + 1, back_shell);}}
        else
          Draw_Pic (vid.width - 44 - x_offset*x_factor, y_value, shell);
      }
    }
    else if (x_offset >= 100 && x_offset <= 200)
    {
      if (GetLowAmmo(cl.stats[STAT_ACTIVEWEAPON], 1) >= cl.stats[STAT_CURRENTMAG])
        {Draw_Pic(vid.width - 245 + x_offset, y_value - 9,emptyshell);
        Draw_AlphaPic(vid.width - 245 + x_offset, y_value - 9,red_shell,alpha);
         Draw_Pic (vid.width - 245 + x_offset + 1, y_value - 8, back_shell);}
      else
        Draw_Pic (vid.width - 245 + x_offset, y_value - 9, shell);
    }
    else if (x_offset > 200 && x_offset <= 298)
    {
      if (GetLowAmmo(cl.stats[STAT_ACTIVEWEAPON], 1) >= cl.stats[STAT_CURRENTMAG])
        {Draw_Pic (vid.width + 154 - x_offset, y_value - 18, emptyshell);
        Draw_AlphaPic(vid.width + 154 - x_offset, y_value - 18,red_shell,alpha);
        Draw_Pic (vid.width + 154 - x_offset + 1, y_value - 17, back_shell);}
      else
        Draw_Pic (vid.width + 154 - x_offset, y_value - 18, shell);
    }
    else
    {
      if (GetLowAmmo(cl.stats[STAT_ACTIVEWEAPON], 1) >= cl.stats[STAT_CURRENTMAG])
        {Draw_AlphaPic(vid.width - 443 + x_offset, y_value - 27,red_shell,alpha);
        Draw_Pic (vid.width - 443 + x_offset, y_value - 27, emptyshell);
        Draw_Pic (vid.width - 443 + x_offset + 1, y_value - 26, back_shell);}
      else
        Draw_Pic (vid.width - 443 + x_offset, y_value - 27, shell);
    }
    maglen = maglen - 1;
 }
  
 while (shelldiff > 0) 
 {
  if (cl.stats[STAT_ACTIVEWEAPON] == 0 || cl.stats[STAT_ACTIVEWEAPON] == 1 || cl.stats[STAT_ACTIVEWEAPON] == 2 || 
      cl.stats[STAT_ACTIVEWEAPON] == 4 || cl.stats[STAT_ACTIVEWEAPON] == 8 || cl.stats[STAT_ACTIVEWEAPON] == 10 || 
      cl.stats[STAT_ACTIVEWEAPON] == 11 || cl.stats[STAT_ACTIVEWEAPON] == 12 || cl.stats[STAT_ACTIVEWEAPON] == 13 || 
      cl.stats[STAT_ACTIVEWEAPON] == 17 || cl.stats[STAT_ACTIVEWEAPON] == 19 || cl.stats[STAT_ACTIVEWEAPON] == 21 || 
      cl.stats[STAT_ACTIVEWEAPON] == 23 || cl.stats[STAT_ACTIVEWEAPON] == 28 || cl.stats[STAT_ACTIVEWEAPON] == 29 || 
      cl.stats[STAT_ACTIVEWEAPON] == 30 || cl.stats[STAT_ACTIVEWEAPON] == 31 || cl.stats[STAT_ACTIVEWEAPON] == 34 || 
      cl.stats[STAT_ACTIVEWEAPON] == 36 || cl.stats[STAT_ACTIVEWEAPON] == 42 || cl.stats[STAT_ACTIVEWEAPON] == 43 || 
      cl.stats[STAT_ACTIVEWEAPON] == 47 || cl.stats[STAT_ACTIVEWEAPON] == 50)
     Draw_Pic (vid.width - 44 + (((shelldiff + backshellnum - 1) * 4) - (backshellnum *8))*x_factor, y_value, emptyshell);
     
     /*if (cl.stats[STAT_ACTIVEWEAPON] == 49 || cl.stats[STAT_ACTIVEWEAPON] == 51)  // upgraded snipers only
      {
        int secondrow;
        
        if (getWeaponMag(cl.stats[STAT_ACTIVEWEAPON]) > 5 && x_offset > 20)
        {
          y_value = vid.height - 26;
          secondrow = 180;
        }
        else
          secondrow = 0;
       
       if (shelldiff <= 4)
        Draw_Pic (vid.width - 44 + (((shelldiff + backshellnum - 1) * 4) - (backshellnum *8))*x_factor + secondrow, y_value, emptyshell);
       else if (shelldiff > 4)
       {
        Draw_Pic (vid.width - 44 + (((shelldiff + backshellnum - 1) * 4) - (backshellnum *8))*x_factor + secondrow, y_value, emptyshell);
       }
      }*/
      
 /* else
    {
      if (maglen <= 33)
        Draw_Pic (vid.width + 20 + ((shelldiff + backshellnum - 1) * 3) - (backshellnum *8) + autooffset, y_value, emptyshell);
      if (maglen > 33 && maglen <= 66)
        Draw_Pic (vid.width + 20 + ((shelldiff + backshellnum - 1) * 3) - (backshellnum *8) + autooffset, y_value - 9, emptyshell);
    }*/
    shelldiff--;
 }
 
 }

	if (GetLowAmmo(cl.stats[STAT_ACTIVEWEAPON], 0) >= cl.stats[STAT_AMMO])
	{
		Draw_ColoredString (vid.width - 42, vid.height - 16, "/", 3);
		Draw_ColoredString (vid.width - 34, vid.height - 16, va ("%i",cl.stats[STAT_AMMO]), 3);
	}
	else
	{
		Draw_Character (vid.width - 42, vid.height - 16, '/');
		Draw_ColoredString (vid.width - 34, vid.height - 16, va ("%i",cl.stats[STAT_AMMO]), 0);
	}
}

/*
===============
HUD_AmmoString
===============
*/

void HUD_AmmoString (void)
{
	int len = 0;
	float alpha;
	
	alpha = 170/100*255;
  float modifier = ((cl.time * 1.7));
  if(modifier < -25.0)
    modifier = -25.0;
    
  if (modifier == -25.0)
    modifier = ((cl.time * -1.7));
    
    alpha += modifier;
    
  if(alpha < 0.0)
	  return;
	    
	
	if (GetLowAmmo(cl.stats[STAT_ACTIVEWEAPON], 1) >= cl.stats[STAT_CURRENTMAG] && cl.stats[STAT_ZOOM] != 2)
	{
		if (0 < cl.stats[STAT_AMMO] && cl.stats[STAT_CURRENTMAG] >= 0) {
			len = 6;
		Draw_AlphaColoredString ((vid.width)/2, (vid.height)/2 + 40, "Reload", 0, alpha);
//	Draw_Pic ((vid.width)/2 - 10, (vid.height)/2 + 40, GetButtonIcon("+reload"));
		} else if (0 < cl.stats[STAT_CURRENTMAG]) {
			len = 8;
		Draw_AlphaColoredString ((vid.width)/2, (vid.height)/2 + 40, "LOW AMMO", 1, alpha);
		} else {
			len = 7;
		Draw_AlphaColoredString ((vid.width)/2, (vid.height)/2 + 40, "NO AMMO", 3, alpha);
		}
	}

	//if (len > 0)
		//Draw_ColoredString ((vid.width)/2, (vid.height)/2 + 40, str, 0);
		//Draw_ColoredString ((vid.width - len*8)/2, (vid.height)/2 + 40, str, 0);
}

/*
===============
HUD_Grenades
===============
*/
#define 	UI_FRAG		1
#define 	UI_BETTY	2

void HUD_Grenades (void)
{
	if (cl.stats[STAT_GRENADES])
	{
		x_value = vid.width - 50;
		y_value = vid.height - 16 - fragpic->height - 4;
	}
	if (cl.stats[STAT_GRENADES] & UI_FRAG)
	{
		Draw_StretchPic (x_value, y_value, fragpic, 22, 22);
		if (cl.stats[STAT_PRIGRENADES] <= 0)
			Draw_ColoredString (x_value + 12, y_value + 12, va ("&cF00%i",cl.stats[STAT_PRIGRENADES]), 0);
		else
			Draw_String (x_value + 12, y_value + 12, va ("%i",cl.stats[STAT_PRIGRENADES]));
	}
	if (cl.stats[STAT_GRENADES] & UI_BETTY)
	{
		Draw_StretchPic (x_value - fragpic->width - 5 + 62, y_value, bettypic, 22, 22);
    if (cl.stats[STAT_SECGRENADES] <= 0)
			Draw_ColoredString (x_value - fragpic->width + 7 + 62, y_value + 12, va ("&cF00%i",cl.stats[STAT_SECGRENADES]), 0);
		else
			Draw_String (x_value - fragpic->width + 7 + 62, y_value + 12, va ("%i",cl.stats[STAT_SECGRENADES]));
	}
}

/*
===============
HUD_Weapon
===============
*/
void HUD_Weapon (void)
{
	char str[32];
	float l;
	x_value = vid.width;
	y_value = vid.height - 16 - fragpic->height - 4 - 16;

	strcpy(str, GetWeaponName(cl.stats[STAT_ACTIVEWEAPON]));
	l = strlen(str);

	x_value = vid.width - 8 - l*8;
	Draw_String (x_value, y_value, str);
}

/*
===============
HUD_Nuke
===============
*/

extern client_t old_nuke;

void HUD_Nuke (void)
{
  qpic_t *nuke;
  scoreboard_t	*s;
  
  s = cl.scores;

  Draw_ColoredString (0,(vid.height - vid.height * 0.45 + 16),va("%i",(host_client->edict->v.nukedelay)), 2);
  nuke = Draw_CacheImg ("gfx/hud/nuke.tga");
  Draw_Pic (vid.width/7, vid.height*5/7, nuke);
}

/*char *ReturnHUD_Char (int whoami)
{
  switch(whoami)
      {
         case 1: return "Dempsey"; break;
         case 2: return "Nikolai"; break;
         case 3: return "Takeo"; break;
         case 4: return "Richthofen"; break;
         default: return "Dempsey"; break;      
      }
}

void HUD_Char(void)
{
    int x, y;
    int whoami;
    char str[32];
    x = vid.width/2;
    y = vid.height/2;

    whoami = (rand() & 3);

    strcpy(str, ReturnHUD_Char(whoami));
	
    Draw_String (x, y, str);
}*/

/*
===============
HUD_Draw
===============
*/
void HUD_Draw (void)
{
  float alpha;
  qpic_t *fadepic;
    
	if (scr_con_current == vid.height)
		return;		// console is full screen

	if (key_dest == key_menu_pause)
		return;
		  
  if (procore.value)
  {
    HUD_Blood();
    return; 
  }

	scr_copyeverything = 1;

	if (waypoint_mode.value)
	{
		Draw_String (vid.width - 112, 0, "WAYPOINTMODE");
		Draw_String (vid.width - 240, 8, "Press fire to create waypoint");
		Draw_String (vid.width - 232, 16, "Press use to select waypoint");
		Draw_String (vid.width - 216, 24, "Press aim to link waypoint");
		Draw_String (vid.width - 248, 32, "Press knife to remove waypoint");
		Draw_String (vid.width - 272, 40, "Press switch to move waypoint here");
		Draw_String (vid.width - 304, 48, "Press reload to make special waypoint");
		return;
	}

	if (cl.stats[STAT_HEALTH] <= 0)
	{
		HUD_EndScreen ();
		return;
	}
	
if (!cls.demoplayback)
 if (fade_screen_time > Sys_FloatTime())
  {
  fadepic = Draw_CacheImg ("gfx/hud/fadeout.png");
  alpha = 170/100*255;
    
  float modifier = (cl.time* 0.5);
    if(modifier < -25.0)
      modifier = 0;

    
    alpha += modifier;
    
    if(alpha < 0.0)
	    return;
	  else
      Draw_AlphaPic(0, 0,fadepic,255 - alpha);
   }
	
if (!cls.demoplayback)
	{
	HUD_Blood();
	HUD_Rounds();
	HUD_ParseWorldspawn();
	HUD_Perks();
	HUD_Powerups();
	
	/*if (gungame.value)
    HUD_Nuke();*/
 // HUD_Char();
   
//	HUD_ProgressBar();
	if ((HUD_Change_time > Sys_FloatTime() || GetLowAmmo(cl.stats[STAT_ACTIVEWEAPON], 1) >= cl.stats[STAT_CURRENTMAG] || GetLowAmmo(cl.stats[STAT_ACTIVEWEAPON], 0) >= cl.stats[STAT_AMMO]) && cl.stats[STAT_HEALTH] >= 10)
	{ //these elements are only drawn when relevant for few seconds
		HUD_Ammo();
		HUD_Grenades();
		HUD_Weapon();
		HUD_AmmoString();
	}
	HUD_Points();
	HUD_Point_Change();
	HUD_Achievement();
	
	if (cl.stats[STAT_HEALTH] <= 20)
  {
  	HUD_ProgressBar();
    return;
  }
 }
}
