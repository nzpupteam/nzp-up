Nazi Zombies: Portable Unofficial Patch 
====================================
##DEVELOPED BY: 
###Ryan Baldwin (Naievil) 
###Pedro Valadés (PerikiyoXD)
###Tyler Young (Scatterbox)

##Summary
----

This is a repo for all of the bug fixing that goes on to fix the mistakes Blubs, Jukki, and Ju[s]tice made when sending out the NZ:P 1.0 Demo. A release will be sent out as more significant work is completed.

##Version
----

Peri thought it would be a good idea to start with a fresh source, instead of having to merge the changes we both worked on. Therefore, we start back at 1.0.

##How to install
----

On PSP

- Plug your PSP
- Go to your PSP directory
- Then, go to PSP/GAME, and install it there.

On PC (Emulator):

- Extract to a folder
- Run the EBOOT.PBP with your emulator (PPSSPP is the recommended one!)

##Troubleshooting
----

If you have any question go http://nzportable.forumotion.com and go ask there, we will answer you asap! (usually 10m - 2h)

##Help & Credits
----

- "Can I help?"
- We mainly rely on ourselves to fix this, but we do speak directly to the devs to help fix bugs faster, so unless you are a Dev or know QuakeC, or C well, no. Also, you have to know how to use Git.

All credit goes to the NZ:P Team, especially in the release of the source code :)